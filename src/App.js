import React from 'react';
import Board from './components/Board';
import './App.css';

function App() {
  return (
    <div className="App">
      <header>
        <h1>React Tic-tac-to</h1>
      </header>
      <Board />
    </div>
  );
}

export default App;
