import React from 'react';
import Box from './Box';
import './Board.css';

class Board extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            turn: 'X'
        }

        this.handleClick = this.handleClick.bind(this)
    };

    handleClick(e) {
        this.setState(function(state, props) {
            return {turn: (state.turn === 'X' ? 'O' : 'X')}
        })
    }

    render() {
        return <div className="board">
            <div className="board-row">
                <Box id="1" turn={this.state.turn} handleClick={this.handleClick} />
                <Box id="2" turn={this.state.turn} handleClick={this.handleClick} />
                <Box id="3" turn={this.state.turn} handleClick={this.handleClick} />
            </div>
            <div className="board-row">
                <Box id="4" turn={this.state.turn} handleClick={this.handleClick} />
                <Box id="5" turn={this.state.turn} handleClick={this.handleClick} />
                <Box id="6" turn={this.state.turn} handleClick={this.handleClick} />
            </div>
            <div className="board-row">
                <Box id="7" turn={this.state.turn} handleClick={this.handleClick} />
                <Box id="8" turn={this.state.turn} handleClick={this.handleClick} />
                <Box id="9" turn={this.state.turn} handleClick={this.handleClick} />
            </div>
        </div>
    };
}

export default Board;