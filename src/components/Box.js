import React from 'react';
import './Box.css';

class Box extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false
        }
        this.handleClick = this.handleClick.bind(this)
    };

    handleClick(e) {
        if(this.state.checked === false) {
            this.setState(function(state, props) {
                return {checked: props.turn}
            })
            this.props.handleClick()
        }
    }

    render() {
        return <div className="box" onClick={this.handleClick}>
            {this.state.checked}
        </div>
    }
}

export default Box;